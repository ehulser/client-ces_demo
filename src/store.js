import { createBrowserHistory } from 'history';
import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import createSagaMiddleware from 'redux-saga';

import saga from './saga';
import createReducer from './reducers';

export const history = createBrowserHistory();
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  createReducer(history),
  applyMiddleware(
    routerMiddleware(history),
    sagaMiddleware,
  ),
);

sagaMiddleware.run(saga);

export default store;
