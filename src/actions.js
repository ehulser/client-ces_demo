import { DEST_VEGAS, DEST_GRAND_CANYON } from './constants';

export const BUILD_ROUTE = 'BUILD_ROUTE';
export const buildRoute = (startName = DEST_VEGAS, destinationName = DEST_GRAND_CANYON) => ({
  destinationName,
  startName,
  type: BUILD_ROUTE,
});

export const REGISTER_USER = 'REGISTER_USER';
export const registerUser = user => ({
  type: REGISTER_USER,
  user,
});

export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const setCurrentUser = user => ({
  type: SET_CURRENT_USER,
  user,
});

export const CAPTURE_WEBCAM = 'CAPTURE_WEBCAM';
export const captureWebcam = image => ({
  image,
  type: CAPTURE_WEBCAM,
});

export const SET_DESTINATION = 'SET_DESTINATION';
export const setDestination = destination => ({
  destination,
  type: SET_DESTINATION,
});

export const SET_FOOD = 'SET_FOOD';
export const setFood = food => ({
  food,
  type: SET_FOOD,
});

export const SET_MEDIA = 'SET_MEDIA';
export const setMedia = media => ({
  media,
  type: SET_MEDIA,
});

export const RESET = 'RESET';
export const reset = () => ({
  type: RESET,
});


export const SET_WAYPOINTS = 'SET_WAYPOINTS';
export const setWaypoints = waypoints => ({
  type: SET_WAYPOINTS,
  waypoints,
});

export const SET_WAYPOINT_LABELS = 'SET_WAYPOINT_LABELS';
export const setWaypointLabels = labels => ({
  labels,
  type: SET_WAYPOINT_LABELS,
});

export const SET_MAP_INFO = 'SET_MAP_INFO';
export const setMapInfo = info => ({
  info,
  type: SET_MAP_INFO,
});

export const SET_DURATIONS = 'SET_DURATIONS';
export const setDurations = durations => ({
  durations,
  type: SET_DURATIONS,
});

export const SET_TRIP_PERCENT_COMPLETE = 'SET_TRIP_PERCENT_COMPLETE';
export const setTripPercentComplete = percent => ({
  percent,
  type: SET_TRIP_PERCENT_COMPLETE,
});

export const SET_TRIP_POSITION = 'SET_TRIP_POSITION';
export const setTripPosition = position => ({
  position,
  type: SET_TRIP_POSITION,
});

export const REQUEST_FOOD_RECOMMENDATIONS = 'REQUEST_FOOD_RECOMMENDATIONS';
export const requestFoodRecommendations = position => ({
  position,
  type: REQUEST_FOOD_RECOMMENDATIONS,
});

export const SET_FOOD_RECOMMENDATIONS = 'SET_FOOD_RECOMMENDATIONS';
export const setFoodRecommendations = recommendations => ({
  recommendations,
  type: SET_FOOD_RECOMMENDATIONS,
});

export const SET_SHOW_RECOMMENDATIONS = 'SET_SHOW_RECOMMENDATIONS';
export const setShowRecommendations = showRecommendations => ({
  showRecommendations,
  type: SET_SHOW_RECOMMENDATIONS,
});

export const ADD_WAYPOINT = 'ADD_WAYPOINT';
export const addWaypoint = (latitude, longitude) => ({
  latitude,
  longitude,
  type: ADD_WAYPOINT,
});
