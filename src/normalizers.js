import _map from 'lodash/map';
import _reduce from 'lodash/reduce';

export const normalizeRouteApiResponse = ({ data }) => {
  const { lat, lon } = data.waypoints;
  const waypoints = _reduce(lat, (acc, latValue, index) => (
    [...acc, [parseInt(latValue, 10), parseInt(lon[index], 10)]]
  ), []);
  const durations = _map(data.durations, d => parseInt(d, 10));
  return { durations, waypoints };
};
