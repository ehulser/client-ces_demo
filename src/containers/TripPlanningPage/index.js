import _reduce from 'lodash/reduce';
import { arrayOf, bool, func, number, string } from 'prop-types';
import { connect } from 'react-redux';
import { MuiThemeProvider } from '@material-ui/core/styles';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';

import {
  addWaypoint,
  buildRoute,
  requestFoodRecommendations,
  setTripPosition,
  setTripPercentComplete,
  setShowRecommendations,
} from '../../actions';
import { DEST_VEGAS } from '../../constants';
import Map from '../../components/Map';
import RecommendationCard, { recommendationType } from '../../components/RecommendationCard';
import MediaPlayer from '../../components/MediaPlayer';
import styles from './styles.module.css';
import theme from './theme';
import TripHeader from '../../components/TripHeader';
import TripTimeline, { waypointType } from '../../components/TripTimeline';

class TripPlanningPage extends React.Component {
  componentDidMount() {
    const { destination, onLoad } = this.props;
    onLoad(DEST_VEGAS, destination || DEST_VEGAS);
  }

  render() {
    const {
      destination,
      firstName,
      onAddRecommendation,
      onPercentComplete,
      onPositionChange,
      onHideRecommendations,
      onWaypointSearch,
      profileImage,
      recommendations,
      soundCloudClientId,
      soundCloudTrackUrl,
      showRecommendations,
      timelineWaypoints,
      waypoints,
    } = this.props;

    return (
      <div className={styles.container}>
        <MuiThemeProvider theme={theme}>
          <div className={styles.header}>
            <TripHeader
              destination={destination}
              firstName={firstName}
              profileImage={profileImage}
            />
            {!!timelineWaypoints.length && <TripTimeline waypoints={timelineWaypoints} />}
          </div>
          <div className={styles.map}>
            {!!waypoints.length && (
              <Map
                onPercentComplete={onPercentComplete}
                onPositionChange={onPositionChange}
                onWaypointSearch={onWaypointSearch}
                showPopup={!showRecommendations}
                waypoints={waypoints}
              />
            )}
          </div>
          {showRecommendations && (
            <div className={styles.recommendations}>
              <div className={styles.recommendationsHeader}>
                <Typography variant="overline">Restaurants Nearby</Typography>
                <div className={styles.stretch} />
                <IconButton onClick={onHideRecommendations}>
                  <Icon>close</Icon>
                </IconButton>
              </div>
              <div className={styles.recommendationCards}>
                {recommendations.map(recommendation => (
                  <RecommendationCard
                    key={recommendation.id}
                    recommendation={recommendation}
                    onAdd={onAddRecommendation}
                  />
                ))}
              </div>
            </div>
          )}
        </MuiThemeProvider>
        <div className={styles.mediaPlayer}>
          <MediaPlayer
            clientId={soundCloudClientId}
            resolveUrl={soundCloudTrackUrl}
          />
        </div>
      </div>
    );
  }
}

TripPlanningPage.propTypes = {
  destination: string,
  firstName: string,
  onAddRecommendation: func.isRequired,
  onHideRecommendations: func.isRequired,
  onLoad: func.isRequired,
  onPercentComplete: func.isRequired,
  onPositionChange: func.isRequired,
  onWaypointSearch: func.isRequired,
  profileImage: string,
  recommendations: arrayOf(recommendationType),
  showRecommendations: bool,
  soundCloudClientId: string.isRequired,
  soundCloudTrackUrl: string.isRequired,
  timelineWaypoints: arrayOf(waypointType),
  waypoints: arrayOf(arrayOf(number)),
};

TripPlanningPage.defaultProps = {
  destination: '',
  firstName: '',
  profileImage: '',
  recommendations: [],
  showRecommendations: false,
  timelineWaypoints: [],
  waypoints: [],
};

const mapStateToProps = ({ map, soundcloud, user }) => {
  const starttime = (new Date()).getTime();
  const { durations, labels, recommendations, showRecommendations, waypoints } = map;
  const lastIndex = waypoints.length - 1;

  let currtime = starttime;
  const timelineWaypoints = _reduce(waypoints, (acc, waypoint, index) => {
    const info = {
      name: labels[index],
      time: currtime,
    };
    if (index > 0 && index < lastIndex) {
      info.duration = 30 * 60 * 1000; // default charge to a half hour
      info.color = '#44CCE1';
    }
    if (index < lastIndex) {
      currtime += durations[index] * 1000;
    }
    return [...acc, info];
  }, []);

  return {
    destination: user.destination,
    firstName: user.firstName,
    profileImage: user.profileImage,
    recommendations,
    showRecommendations,
    soundCloudClientId: soundcloud.clientId,
    soundCloudTrackUrl: soundcloud.currentTrackUrl,
    timelineWaypoints,
    waypoints: map.waypoints,
  };
};

const mapDispatchToProps = dispatch => ({
  onAddRecommendation: ({ latitude, longitude }) => dispatch(addWaypoint(latitude, longitude)),
  onHideRecommendations: () => dispatch(setShowRecommendations(false)),
  onLoad: (start, end) => dispatch(buildRoute(start, end)),
  onPercentComplete: percent => dispatch(setTripPercentComplete(percent)),
  onPositionChange: position => dispatch(setTripPosition(position)),
  onWaypointSearch: position => dispatch(requestFoodRecommendations(position)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TripPlanningPage);
