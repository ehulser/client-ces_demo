import { arrayOf, objectOf, bool, string, shape, func } from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import cs from 'classnames/bind';

import { reset, setFood } from '../../actions';
import BackButton from '../../components/BackButton';
import bg from './background.png';
import chinese from './img/chinese.png';
import FancyButton from '../../components/FancyButton';
import italian from './img/italian.png';
import japanese from './img/japanese.png';
import Logo from '../../components/Logo';
import mediterranean from './img/mediterranean.png';
import mexican from './img/mexican.png';
import ProgressBar from '../../components/ProgressBar';
import seafood from './img/seafood.png';
import spicy from './img/spicy.png';
import styles from './styles.module.css';
import vegan from './img/vegan.png';

const cx = cs.bind(styles);

class FoodPage extends React.Component {
  constructor(props) {
    super(props);

    const { food } = props;
    this.state = { food };
  }

  toggle(title) {
    const { food } = this.state;
    this.setState({ food: { ...food, [title]: !food[title] } });
  }

  render() {
    const { onGoBack, onGoHome, onSubmit, tiles } = this.props;
    const { food } = this.state;
    return (
      <div className={styles.container} style={{ backgroundImage: `url(${bg})` }}>
        <div className={styles.header}>
          <BackButton onClick={onGoBack} />
          <div className={styles.spacer} />
          <ProgressBar value={4} />
          <IconButton onClick={onGoHome}>
            <Icon>refresh</Icon>
          </IconButton>
        </div>
        <div className={styles.content}>
          <div className={styles.copy}>
            <Typography variant="h2" className={styles.title}>
              WHAT ARE YOUR FAVORITE FOODS?
            </Typography>
          </div>
          <div className={styles.tiles}>
            <div className={styles.frame} />
            {tiles.map(({ image, title }) => (
              <div key={title} className={styles.tile} style={{ backgroundImage: `url(${image})` }}>
                <div className={cx('favico', { checked: food[title] })}>
                  <Button onClick={() => this.toggle(title)} variant="fab" size="small">
                    <Icon className={cx('icon', { checked: food[title] })}>favorite</Icon>
                  </Button>
                </div>
                <Typography variant="h6">{title}</Typography>
              </div>
            ))}
          </div>
        </div>
        <div className={styles.footer}>
          <Logo />
          <div className={styles.spacer} />
          <FancyButton onClick={() => onSubmit(food)}>
            Next
          </FancyButton>
        </div>
      </div>
    );
  }
}

FoodPage.propTypes = {
  food: objectOf(bool),
  onGoBack: func.isRequired,
  onGoHome: func.isRequired,
  onSubmit: func.isRequired,
  tiles: arrayOf(shape({
    image: string,
    title: string,
  })),
};

FoodPage.defaultProps = {
  food: {
    CHINESE: false,
    ITALIAN: false,
    JAPANESE: false,
    MEDITERRANEAN: false,
    MEXICAN: false,
    SEAFOOD: false,
    SPICY: false,
    VEGAN: false,
  },
  tiles: [{
    image: italian,
    title: 'ITALIAN',
  }, {
    image: spicy,
    title: 'SPICY',
  }, {
    image: mexican,
    title: 'MEXICAN',
  }, {
    image: chinese,
    title: 'CHINESE',
  }, {
    image: seafood,
    title: 'SEAFOOD',
  }, {
    image: vegan,
    title: 'VEGAN',
  }, {
    image: japanese,
    title: 'JAPANESE',
  }, {
    image: mediterranean,
    title: 'MEDITERRANEAN',
  }],
};

const mapStateToProps = ({ user }) => ({
  food: user.food,
});

const mapDispatchToProps = dispatch => ({
  onGoBack: () => dispatch(push('/media')),
  onGoHome: () => dispatch(reset()),
  onSubmit: (food) => {
    dispatch(setFood(food));
    dispatch(push('/exit'));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(FoodPage);
