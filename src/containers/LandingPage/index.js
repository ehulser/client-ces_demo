import { connect } from 'react-redux';
import { func } from 'prop-types';
import { push } from 'connected-react-router';
import React from 'react';
import Typography from '@material-ui/core/Typography';

import FancyButton from '../../components/FancyButton';
import bg from './background.png';
import Logo from '../../components/Logo';
import styles from './styles.module.css';

const LandingPage = ({ onGoNext }) => (
  <div className={styles.container} style={{ backgroundImage: `url(${bg})` }}>
    <div className={styles.copy}>
      <Typography variant="h4">
        Welcome to
      </Typography>
      <Typography variant="h2" className={styles.title}>
        BYTON TRIP PLANNING STATION
      </Typography>
      <Typography className={styles.paragraph}>
        At BYTON, we believe a better planning enhances more joy and relaxing.
        For every road trip you take with BYTON, we want you to have the most
        seamless experience at every moment.
      </Typography>
    </div>
    <div className={styles.footer}>
      <Logo />
      <div className={styles.spacer} />
      <FancyButton
        onClick={onGoNext}
        variant="contained"
      >
        Start
      </FancyButton>
    </div>
  </div>
);

LandingPage.propTypes = {
  onGoNext: func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  onGoNext: () => dispatch(push('/scan')),
});

export default connect(null, mapDispatchToProps)(LandingPage);
