import { arrayOf, objectOf, bool, string, shape, func } from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import cs from 'classnames/bind';

import { setMedia } from '../../actions';
import BackButton from '../../components/BackButton';
import bg from './background.png';
import chill from './img/chill.png';
import classic from './img/classic.png';
import country from './img/country.png';
import dance from './img/dance.png';
import FancyButton from '../../components/FancyButton';
import hiphop from './img/hiphop.png';
import jazz from './img/jazz.png';
import Logo from '../../components/Logo';
import pop from './img/pop.png';
import ProgressBar from '../../components/ProgressBar';
import rock from './img/rock.png';
import styles from './styles.module.css';

const cx = cs.bind(styles);

class MediaPage extends React.Component {
  constructor(props) {
    super(props);

    const { media } = props;
    this.state = { media };
  }

  toggle(title) {
    const { media } = this.state;
    this.setState({ media: { ...media, [title]: !media[title] } });
  }

  render() {
    const { onGoBack, onGoHome, onSubmit, tiles } = this.props;
    const { media } = this.state;
    return (
      <div className={styles.container} style={{ backgroundImage: `url(${bg})` }}>
        <div className={styles.header}>
          <BackButton onClick={onGoBack} />
          <div className={styles.spacer} />
          <ProgressBar value={4} />
          <IconButton onClick={onGoHome}>
            <Icon>refresh</Icon>
          </IconButton>
        </div>
        <div className={styles.content}>
          <div className={styles.copy}>
            <Typography variant="h2" className={styles.title}>
              WHAT KIND OF STUFF ARE YOU LISTENING TO?
            </Typography>
          </div>
          <div className={styles.tiles}>
            <div className={styles.frame} />
            {tiles.map(({ image, title }) => (
              <div key={title} className={styles.tile} style={{ backgroundImage: `url(${image})` }}>
                <div className={cx('favico', { checked: media[title] })}>
                  <Button onClick={() => this.toggle(title)} variant="fab" size="small">
                    <Icon className={cx('icon', { checked: media[title] })}>favorite</Icon>
                  </Button>
                </div>
                <Typography variant="h6">{title}</Typography>
              </div>
            ))}
          </div>
        </div>
        <div className={styles.footer}>
          <Logo />
          <div className={styles.spacer} />
          <FancyButton onClick={() => onSubmit(media)}>
            Next
          </FancyButton>
        </div>
      </div>
    );
  }
}

MediaPage.propTypes = {
  media: objectOf(bool),
  onGoBack: func.isRequired,
  onGoHome: func.isRequired,
  onSubmit: func.isRequired,
  tiles: arrayOf(shape({
    image: string,
    title: string,
  })),
};

MediaPage.defaultProps = {
  media: {
    CHILL: false,
    CLASSIC: false,
    COUNTRY: false,
    DANCE: false,
    'HIP-HOP': false,
    JAZZ: false,
    POP: false,
    ROCK: false,
  },
  tiles: [{
    image: jazz,
    title: 'JAZZ',
  }, {
    image: pop,
    title: 'POP',
  }, {
    image: rock,
    title: 'ROCK',
  }, {
    image: country,
    title: 'COUNTRY',
  }, {
    image: classic,
    title: 'CLASSIC',
  }, {
    image: dance,
    title: 'DANCE',
  }, {
    image: hiphop,
    title: 'HIP-HOP',
  }, {
    image: chill,
    title: 'CHILL',
  }],
};

const mapStateToProps = ({ user }) => ({
  media: user.media,
});

const mapDispatchToProps = dispatch => ({
  onGoBack: () => dispatch(push('/destination')),
  onGoHome: () => dispatch(push('/')),
  onSubmit: (media) => {
    dispatch(setMedia(media));
    dispatch(push('/food'));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MediaPage);
