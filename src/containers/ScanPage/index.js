import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { string, func } from 'prop-types';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import React, { Fragment } from 'react';
import Typography from '@material-ui/core/Typography';

import { captureWebcam } from '../../actions';
import BackButton from '../../components/BackButton';
import ConnectedScanButton from '../ConnectedScanButton';
import FancyButton from '../../components/FancyButton';
import Logo from '../../components/Logo';
import ProgressBar from '../../components/ProgressBar';
import styles from './styles.module.css';


class ScanPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { percent: 0 };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(percent) {
    this.setState({ percent });
  }

  render() {
    const { percent } = this.state;
    const {
      ageGroup,
      firstName,
      gender,
      lastName,
      onCapture,
      onGoBack,
      onGoHome,
      onSubmit,
    } = this.props;

    return (
      <div className={styles.container}>
        <div className={styles.header}>
          <BackButton onClick={onGoBack} />
          <div className={styles.stretch} />
          <ProgressBar value={1} />
          <IconButton onClick={onGoHome}>
            <Icon>refresh</Icon>
          </IconButton>
        </div>
        <Typography variant="h2" className={styles.title}>
          FACIAL SCANING
        </Typography>
        <div className={styles.content}>
          <div className={styles.copy}>
            <Typography variant="h4">
              {percent === 0 && 'Positioning'}
              {percent === 100 && !!firstName && 'Recognized'}
              {percent === 100 && !firstName && 'Unrecognized'}
              {percent !== 0 && percent !== 100 && 'Scanning...'}
            </Typography>
            <Typography className={styles.paragraph}>
              {percent === 0 && 'Look at the screen and press START to start scanning'}
              {percent === 100 && !!firstName && 'Now you can be able to access your trip planning at anytime'}
              {percent === 100 && !firstName && 'Please tell us a bit about yourself'}
              {percent !== 0 && percent !== 100 && 'Keep position until reach 100%'}
            </Typography>
          </div>
          <div className={styles.stretch}>
            <ConnectedScanButton onCapture={onCapture} onChange={this.handleChange} />
          </div>
          <div className={styles.user}>
            {percent === 100 && (
              <Fragment>
                <Typography variant="h4">
                  {firstName} {lastName}
                </Typography>
                <Typography className={styles.paragraph}>
                  {ageGroup}
                  <br />
                  {gender}
                </Typography>
              </Fragment>
            )}
          </div>
        </div>
        <div className={styles.footer}>
          <Logo />
          <div className={styles.stretch} />
          <FancyButton
            disabled={percent !== 100}
            onClick={onSubmit}
          >
            Next
          </FancyButton>
        </div>
      </div>
    );
  }
}

ScanPage.propTypes = {
  ageGroup: string,
  firstName: string,
  gender: string,
  lastName: string,
  onCapture: func.isRequired,
  onGoBack: func.isRequired,
  onGoHome: func.isRequired,
  onSubmit: func.isRequired,
};

ScanPage.defaultProps = {
  ageGroup: '',
  firstName: '',
  gender: '',
  lastName: '',
};

const mapStateToProps = ({ user }) => ({
  ageGroup: user.ageGroup,
  firstName: user.firstName,
  gender: user.gender,
  lastName: user.lastName,
});

const mapDispatchToProps = dispatch => ({
  onCapture: image => dispatch(captureWebcam(image)),
  onGoBack: () => dispatch(push('/')),
  onGoHome: () => dispatch(push('/')),
  onSubmit: () => dispatch(push('/register')),
});

export default connect(mapStateToProps, mapDispatchToProps)(ScanPage);
