import { connect } from 'react-redux';
import { func, string } from 'prop-types';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import { push } from 'connected-react-router';

import bg from './background.png';
import FancyButton from '../../components/FancyButton';
import Logo from '../../components/Logo';
import styles from './styles.module.css';

const ExitPage = ({ destination, firstName, onSubmit }) => (
  <div className={styles.container} style={{ backgroundImage: `url(${bg})` }}>
    <div className={styles.content}>
      <div className={styles.copy}>
        <Typography>
          Thank you {firstName}!
        </Typography>
        <Typography variant="h2" className={styles.title}>
          YOUR TRIP TO {destination.toUpperCase()} HAS BEEN PLANNED
        </Typography>
        <Typography className={styles.paragraph}>
          Please pass by AI EXHIBITION at anytime during your BYTON visit
          to see your planned trip.  In order to have quick access to enjoy
          a BYTON interior, you need to print out your trip boarding pass.
        </Typography>
      </div>
    </div>
    <div className={styles.footer}>
      <Logo />
      <div className={styles.spacer} />
      <FancyButton
        onClick={onSubmit}
      >
        Go to my trip
      </FancyButton>
    </div>
  </div>
);

ExitPage.propTypes = {
  destination: string,
  firstName: string,
  onSubmit: func.isRequired,
};

ExitPage.defaultProps = {
  destination: '',
  firstName: '',
};

const mapStateToProps = ({ user }) => ({
  destination: user.destination,
  firstName: user.firstName,
});

const mapDispatchToProps = dispatch => ({
  onSubmit: () => dispatch(push('/trip')),
});

export default connect(mapStateToProps, mapDispatchToProps)(ExitPage);
