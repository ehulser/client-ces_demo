import { arrayOf, shape, func, string } from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import React from 'react';
import Typography from '@material-ui/core/Typography';

import antelopeCanyon from './img/antelope-canyon.jpg';
import grandCanyon from './img/grand-canyon.jpg';
import hooverDam from './img/hoover-dam.jpg';

import { setDestination } from '../../actions';
import { DEST_HOOVER_DAM, DEST_GRAND_CANYON, DEST_ANTELOPE_CANYON } from '../../constants';
import BackButton from '../../components/BackButton';
import FancyButton from '../../components/FancyButton';
import Logo from '../../components/Logo';
import ProgressBar from '../../components/ProgressBar';
import styles from './styles.module.css';

class DestinationPage extends React.Component {
  constructor(props) {
    super(props);
    let index = 1;
    props.copy.forEach((entry, i) => {
      if (entry.title === props.destination) {
        index = i + 1;
      }
    });

    this.state = {
      page: index,
    };
    this.goBack = this.goBack.bind(this);
    this.goForward = this.goForward.bind(this);
  }

  goBack() {
    const { page } = this.state;
    this.setState({ page: page - 1 });
  }

  goForward() {
    const { page } = this.state;
    this.setState({ page: page + 1 });
  }

  render() {
    const { page } = this.state;
    const { copy, onGoBack, onGoHome, onSubmit } = this.props;

    return (
      <div className={styles.container}>
        <div className={styles.image} style={{ backgroundImage: `url(${copy[page - 1].image})` }} />
        <div className={styles.inner}>
          <div className={styles.header}>
            <BackButton onClick={onGoBack} />
            <div className={styles.spacer} />
            <ProgressBar value={3} />
            <IconButton onClick={onGoHome}>
              <Icon>refresh</Icon>
            </IconButton>
          </div>
          <div className={styles.content}>
            <div className={styles.copy}>
              <Typography variant="h2" className={styles.title}>
                WHERE IS YOUR NEXT ADVENTURE?
              </Typography>
              <Typography variant="overline" className={styles.pageBar}>
                <IconButton onClick={this.goBack} disabled={page === 1}>
                  <Icon>arrow_left</Icon>
                </IconButton>
                <span>{page} of {copy.length}</span>
                <IconButton onClick={this.goForward} disabled={page === copy.length}>
                  <Icon>arrow_right</Icon>
                </IconButton>
              </Typography>
              <Typography variant="h4">
                {copy[page - 1].title}
              </Typography>
              <Typography className={styles.paragraph}>
                {copy[page - 1].text}
              </Typography>
            </div>
          </div>
          <div className={styles.footer}>
            <Logo />
            <div className={styles.spacer} />
            <FancyButton onClick={() => onSubmit(copy[page - 1].title)}>
              Next
            </FancyButton>
          </div>
        </div>
      </div>
    );
  }
}

DestinationPage.propTypes = {
  copy: arrayOf(shape({
    image: string,
    text: string,
    title: string,
  })),
  destination: string,
  onGoBack: func.isRequired,
  onGoHome: func.isRequired,
  onSubmit: func.isRequired,
};

DestinationPage.defaultProps = {
  copy: [{
    image: grandCanyon,
    text: `Grand Canyon National Park, in Arizona, is home to
    much of the immense Grand Canyon, with its layered
    bands of red rock revealing millions of years of
    geological history.`,
    title: DEST_GRAND_CANYON,
  }, {
    image: antelopeCanyon,
    text: `Antelope Canyon is without a doubt one of the most
    sought-after and high-demand tourist destinations in the area, perhaps
    the entire American Southwest.`,
    title: DEST_ANTELOPE_CANYON,
  }, {
    image: hooverDam,
    text: `Hoover Dam is a concrete arch-gravity dam in the Black Canyon
    of the Colorado River, on the border between the U.S. states of Nevada
    and Arizona.`,
    title: DEST_HOOVER_DAM,
  }],
  destination: DEST_GRAND_CANYON,
};

const mapStateToProps = ({ user }) => ({
  destination: user.destination,
});

const mapDispatchToProps = dispatch => ({
  onGoBack: () => dispatch(push('/register')),
  onGoHome: () => dispatch(push('/')),
  onSubmit: (destination) => {
    dispatch(setDestination(destination));
    dispatch(push('/media'));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(DestinationPage);
