import { func } from 'prop-types';
import React from 'react';

import ScanButton from '../../components/ScanButton';

class ConnectedScanButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      percent: 0,
    };
    this.webcam = null;

    this.setWebcam = this.setWebcam.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  setWebcam(webcam) {
    this.webcam = webcam;
  }

  handleClick() {
    const { onChange, onCapture } = this.props;
    if (onChange) onChange(0);
    if (onCapture) onCapture(this.webcam.getScreenshot());
    this.setState({ percent: 0 });
    this.timerId = setInterval(() => {
      const { percent } = this.state;
      if (percent === 100) {
        clearInterval(this.timerId);
      } else {
        if (onChange) onChange(percent + 1);
        if (onCapture) onCapture(this.webcam.getScreenshot());
        this.setState({ percent: percent + 1 });
      }
    }, 50);
  }

  render() {
    const { percent } = this.state;
    return (
      <ScanButton
        percent={percent}
        onClick={this.handleClick}
        onRef={this.setWebcam}
      />
    );
  }
}

ConnectedScanButton.propTypes = {
  onCapture: func,
  onChange: func,
};

ConnectedScanButton.defaultProps = {
  onCapture: null,
  onChange: null,
};

export default ConnectedScanButton;
