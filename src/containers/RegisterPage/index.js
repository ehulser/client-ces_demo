import { connect } from 'react-redux';
import { Formik } from 'formik';
import { func } from 'prop-types';
import { push } from 'connected-react-router';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import React, { Fragment } from 'react';
import Typography from '@material-ui/core/Typography';

import { registerUser } from '../../actions';
import FancyButton from '../../components/FancyButton';
import Logo from '../../components/Logo';
import ProgressBar from '../../components/ProgressBar';
import RegisterForm from '../../components/RegisterForm';
import BackButton from '../../components/BackButton';
import styles from './styles.module.css';
import bg from './background.png';

const RegisterPage = ({ defaults, goBack, goHome, onSubmit }) => (
  <div className={styles.container} style={{ backgroundImage: `url(${bg})` }}>
    <Formik initialValues={defaults}>
      {({ values, handleChange }) => (
        <Fragment>
          <div className={styles.header}>
            <BackButton onClick={goBack} />
            <div className={styles.spacer} />
            <ProgressBar value={2} />
            <IconButton onClick={goHome}>
              <Icon>refresh</Icon>
            </IconButton>
          </div>
          <div className={styles.content}>
            <div className={styles.copy}>
              <Typography variant="h2" className={styles.title}>
                YOUR UNIQUE FIGURES FORM THE BEST ROUTES
              </Typography>
              <Typography className={styles.paragraph}>
                We value and secure your inputs to create the best values
                of your beliefs: Yourself, your experience.
              </Typography>
            </div>
            <div className={styles.form}>
              <RegisterForm values={values} onChange={handleChange} />
            </div>
          </div>
          <div className={styles.footer}>
            <Logo />
            <div className={styles.spacer} />
            <FancyButton
              disabled={!RegisterForm.isComplete(values)}
              onClick={() => onSubmit(values)}
            >
              Next
            </FancyButton>
          </div>
        </Fragment>
      )}
    </Formik>
  </div>
);

RegisterPage.propTypes = {
  defaults: RegisterForm.propTypes.values,
  goBack: func.isRequired,
  goHome: func.isRequired,
  onSubmit: func.isRequired,
};

RegisterPage.defaultProps = {
  defaults: RegisterForm.defaultProps.values,
};

const mapStateToProps = ({ user }) => ({ defaults: user });

const mapDispatchToProps = dispatch => ({
  goBack: () => dispatch(push('/scan')),
  goHome: () => dispatch(push('/')),
  onSubmit: user => dispatch(registerUser(user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);
