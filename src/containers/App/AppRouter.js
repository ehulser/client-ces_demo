import { ConnectedRouter } from 'connected-react-router';
import { object } from 'prop-types';
import { Route } from 'react-router-dom';
import React, { Fragment } from 'react';

import DestinationPage from '../DestinationPage';
import ExitPage from '../ExitPage';
import FoodPage from '../FoodPage';
import LandingPage from '../LandingPage';
import MediaPage from '../MediaPage';
import RegisterPage from '../RegisterPage';
import ScanPage from '../ScanPage';
import TripPlanningPage from '../TripPlanningPage';

const AppRouter = ({ history }) => (
  <ConnectedRouter history={history}>
    <Fragment>
      <Route exact path="/" component={LandingPage} />
      <Route exact path="/destination" component={DestinationPage} />
      <Route exact path="/exit" component={ExitPage} />
      <Route exact path="/food" component={FoodPage} />
      <Route exact path="/media" component={MediaPage} />
      <Route exact path="/register" component={RegisterPage} />
      <Route exact path="/scan" component={ScanPage} />
      <Route exact path="/trip" component={TripPlanningPage} />
    </Fragment>
  </ConnectedRouter>
);

AppRouter.propTypes = {
  history: object.isRequired,
};

export default AppRouter;
