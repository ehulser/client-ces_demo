import { ApolloProvider } from 'react-apollo';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { object } from 'prop-types';
import { Provider, connect } from 'react-redux';
import ApolloClient from 'apollo-boost';
import React from 'react';

import AppRouter from './AppRouter';
import theme from './theme';

const client = new ApolloClient({
  uri: process.env.REACT_APP_GRAPHQL_URI || 'http://localhost:4000',
});

const App = ({
  history,
  store,
}) => (
  <Provider store={store}>
    <ApolloProvider client={client}>
      <MuiThemeProvider theme={theme}>
        <AppRouter history={history} />
      </MuiThemeProvider>
    </ApolloProvider>
  </Provider>
);

App.propTypes = {
  history: object.isRequired,
  store: object.isRequired,
};

export default connect()(App);
