import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    text: {
      primary: '#dcdcdc',
      secondary: '#dcdcdc',
    },
    type: 'dark',
  },
  typography: {
    fontFamily: 'Helvetica',
    fontSize: '11px',
    h2: {
      fontSize: '3.5em',
    },
    h4: {
      fontSize: '1.8em',
      fontWeight: 300,
    },
    letterSpacing: '2px',
  },
});

export default theme;
