// D
export const DEST_ANTELOPE_CANYON = 'Antelope Canyon';
export const DEST_GRAND_CANYON = 'Grand Canyon National Park';
export const DEST_HOOVER_DAM = 'Hoover Dam Bridge';
export const DEST_VEGAS = 'Las Vegas Convention Center';
