import axios from 'axios';

const SERVER_IP = '35.233.131.242';
const WAYPOINT_URL = `http://${SERVER_IP}:18080/params`;
const FOOD_URL = `http://${SERVER_IP}:5001/getfoodjson`;

const MOCK_WAYPOINT_RESPONSE = {
  data: {
    durations: ['7859.399902', '11498.299805'],
    waypoints: {
      lat: ['36.169900', '35.211544', '36.107000'],
      lon: ['-115.139800', '-114.017059', '-112.113000'],
    },
  },
  status: 200,
};

export const getMockWaypoints = () => (
  MOCK_WAYPOINT_RESPONSE
);

export const getWaypoints = async (start, dest) => (
  axios.get(WAYPOINT_URL, {
    params: {
      lat1: start.latitude,
      lat2: dest.latitude,
      lon1: start.longitude,
      lon2: dest.longitude,
    },
    timeout: 5000,
  })
);

export const getFoodRecommendations = async (latitude, longitude, favorite) => (
  axios.get('/getfoodjson', {
    params: {
      favorite: favorite.join(','),
      latitude,
      longitude,
    },
    timeout: 5000,
  })
);
