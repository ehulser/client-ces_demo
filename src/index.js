import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';

import './setup';
import store, { history } from './store';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App history={history} store={store} />, document.getElementById('root'));
registerServiceWorker();
