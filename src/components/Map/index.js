import { arrayOf, bool, func, number } from 'prop-types';
import { Map as LeafletMap, TileLayer, Marker } from 'react-leaflet';
import L from 'leaflet';
import React from 'react';
import 'leaflet/dist/leaflet.css';

import RoutingMachine from './RoutingMachine';
import WaypointMarker from './WaypointMarker';
import styles from './styles.module.css';

const DEFAULT_COORDS = [-115.1398, 36.1699];

const orangeIcon = new L.Icon({
  iconAnchor: [6, 6],
  iconSize: [12, 12],
  iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Location_dot_orange.svg/2000px-Location_dot_orange.svg.png',
});

const blueIcon = new L.Icon({
  iconAnchor: [6, 6],
  iconSize: [12, 12],
  iconUrl: 'https://www.finecut.co.uk/images/product-listing/product-8266.png',
});

class Map extends React.Component {
  constructor(props) {
    super(props);
    this.map = React.createRef();
    this.state = { position: null };
    this.startAnimation = this.startAnimation.bind(this);
  }

  startAnimation(e) {
    const { routes } = e;
    const { coordinates } = routes[0];
    const { onPercentComplete, onPositionChange } = this.props;
    let index = 0;
    let position = coordinates[index];
    this.setState({ position });
    if (onPercentComplete) onPercentComplete((index / coordinates.length) * 100);
    if (onPositionChange) onPositionChange(position);
    const id = setInterval(() => {
      index += 1;
      if (onPercentComplete) onPercentComplete((index / coordinates.length) * 100);
      if (index === coordinates.length) {
        clearInterval(id);
      } else {
        position = coordinates[index];
        this.setState({ position });
        if (onPositionChange) onPositionChange(position);
      }
    }, 1);
  }

  render() {
    const { onWaypointSearch, showPopup, waypoints, zoom } = this.props;
    const { position } = this.state;
    const start = waypoints[0] || DEFAULT_COORDS;
    const destination = waypoints[waypoints.length - 1] || DEFAULT_COORDS;
    return (
      <LeafletMap
        center={[(destination[0] + start[0]) / 2, (destination[1] + start[1]) / 2]}
        className={styles.container}
        ref={this.map}
        zoom={zoom}
      >
        <TileLayer
          attribution="&copy; <a href=&quot;http://www.openstreetmap.org/copyright&quot;>OpenStreetMap</a> &copy; <a href=&quot;https://carto.com/attributions&quot;>CARTO</a>"
          url="https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png"
          subdomains="abcd"
        />
        {!!waypoints.length && (
          <RoutingMachine
            key={waypoints.length}
            color="#000"
            waypoints={waypoints}
            destination={destination}
            map={this.map}
            onRoutesFound={() => null}
          />
        )}
        {position !== null && <Marker position={position} icon={blueIcon} />}
        {waypoints.map(pos => (
          <WaypointMarker onSearch={onWaypointSearch} position={pos} showPopup={showPopup} />
        ))}
      </LeafletMap>
    );
  }
}

Map.propTypes = {
  onPercentComplete: func,
  onPositionChange: func,
  onWaypointSearch: func,
  showPopup: bool,
  waypoints: arrayOf(arrayOf(number)),
  zoom: number,
};

Map.defaultProps = {
  onPercentComplete: null,
  onPositionChange: null,
  onWaypointSearch: null,
  showPopup: true,
  waypoints: [],
  zoom: 8,
};

export default Map;
