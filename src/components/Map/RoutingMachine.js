import { MapLayer, withLeaflet } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet-control-geocoder/src';
import 'leaflet-routing-machine/dist/leaflet-routing-machine.css';
import 'leaflet-routing-machine/src';

class RoutingMachine extends MapLayer {
  createLeafletElement() {
    const {
      color,
      map,
      onRoutesFound,
      waypoints,
    } = this.props;

    const leafletElement = L.Routing
      .control({
        addWaypoints: false,
        altLineOptions: { styles: [{ opacity: 0 }] },
        createMarker: () => null,
        draggableWaypoints: false,
        fitSelectedRoutes: false,
        geocoder: L.Control.Geocoder.nominatim(),
        lineOptions: {
          styles: [{
            color,
            opacity: 0.8,
            weight: 2,
          }],
        },
        reverseWaypoints: true,
        routeWhileDragging: true,
        serviceUrl: 'http://35.233.131.242:5000/route/v1',
        showAlternatives: false,
        waypoints,
      })
      .addTo(map.current.leafletElement);

    leafletElement.on('routesfound', onRoutesFound);
    leafletElement.hide(); // hide road describtion

    return leafletElement.getPlan();
  }
}

export default withLeaflet(RoutingMachine);
