import { arrayOf, bool, func, number, string } from 'prop-types';
import { Marker, Popup } from 'react-leaflet';
import Icon from '@material-ui/core/Icon';
import L from 'leaflet';
import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import bytonCharge from './byton.png';
import styles from './styles.module.css';


const WAYPOINT_ICON = new L.Icon({
  iconAnchor: [6, 20],
  iconSize: [12, 20],
  iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Map_marker.svg/667px-Map_marker.svg.png',
  popupAnchor: [1, -34],
  shadowSize: [20, 20],
  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
});


const WaypointMarker = ({
  city,
  image,
  onSearch,
  position,
  postalCode,
  showPopup,
  state,
  street,
  title,
  type,
}) => (
  <Marker position={position} icon={WAYPOINT_ICON}>
    {showPopup && (
      <Popup className={styles.popup}>
        <div className={styles.row}>
          <div className={styles.image}>
            <img src={image} width={192} height={192} alt="" />
          </div>
          <div className={styles.addressInfo}>
            <Typography variant="overline">{type}</Typography>
            <Typography variant="h5">{title}</Typography>
            <Typography className={styles.paragraph}>
              {street} <br />
              {city}, {state} {postalCode}
            </Typography>
            <div className={styles.row}>
              <Icon className={styles.spaceRight}>access_time</Icon>
              <Typography className={styles.marginFix}>
                24/7
              </Typography>
            </div>
            <div className={styles.row}>
              <Icon className={styles.spaceRight}>flash_on</Icon>
              <Typography className={styles.marginFix}>
                10 Stations
              </Typography>
            </div>
          </div>
        </div>
        <div className={styles.summary}>
          <div className={styles.summaryCard}>
            <Icon>airport_shuttle</Icon>
            <Typography variant="overline">ARRIVAL</Typography>
            <Typography variant="h6">58 <small>mi</small></Typography>
          </div>
          <div className={styles.summaryCard}>
            <Icon>access_time</Icon>
            <Typography variant="overline">CHARGE</Typography>
            <Typography variant="h6">28 <small>mins</small></Typography>
          </div>
          <div className={styles.summaryCard}>
            <Icon>airport_shuttle</Icon>
            <Typography variant="overline">DEPART</Typography>
            <Typography variant="h6">288 <small>mi</small></Typography>
          </div>
        </div>
        <div className={styles.fab}>
          <Button variant="fab" color="primary" onClick={() => onSearch(position)}>
            <Icon>search</Icon>
          </Button>
        </div>
      </Popup>
    )}
  </Marker>
);

WaypointMarker.propTypes = {
  city: string,
  image: string,
  onSearch: func,
  position: arrayOf(arrayOf(number)).isRequired,
  postalCode: string,
  showPopup: bool,
  state: string,
  street: string,
  title: string,
  type: string,
};

WaypointMarker.defaultProps = {
  city: 'Dolan Spring',
  image: bytonCharge,
  onSearch: null,
  postalCode: '89502',
  showPopup: true,
  state: 'NV',
  street: '888 Charge Rd.',
  title: 'BYTON FAST CHARGE',
  type: 'EV CHARGING STATION',
};

export default WaypointMarker;
