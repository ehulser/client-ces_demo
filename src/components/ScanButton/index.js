import 'react-circular-progressbar/dist/styles.css';
import { func, number } from 'prop-types';
import Button from '@material-ui/core/Button';
import CircularProgressbar from 'react-circular-progressbar';
import React from 'react';
import Webcam from 'react-webcam';
import cs from 'classnames/bind';

import styles from './styles.module.css';

const cx = cs.bind(styles);

const ScanButton = ({ onClick, onRef, percent }) => (
  <div className={styles.container}>
    <div className={styles.webcamContainer}>
      <Webcam
        className={cx('webcam', { disabled: percent === 0 })}
        screenshotFormat="image/jpeg"
        ref={onRef}
      />
    </div>
    <CircularProgressbar
      className={styles.progress}
      percentage={percent}
      strokeWidth={0.5}
      styles={{
        path: { stroke: '#79564B' },
      }}
    />
    <Button className={styles.button} onClick={onClick}>
      {percent === 0 && <span className={styles.title}>START SCANNING</span>}
      {percent > 0 && (
        <span className={styles.percent}>
          {percent}%
        </span>
      )}
    </Button>
  </div>
);

ScanButton.propTypes = {
  onClick: func.isRequired,
  onRef: func,
  percent: number,
};

ScanButton.defaultProps = {
  onRef: null,
  percent: 0,
};

export default ScanButton;
