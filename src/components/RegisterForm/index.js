import { Link } from 'react-router-dom';
import { func, string, shape, bool } from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import React from 'react';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';

import styles from './styles.module.css';

const RegisterForm = ({
  onChange,
  values,
}) => (
  <form className={styles.container}>
    <div className={styles.form}>
      <div className={styles.frame} />
      <div className={styles.row}>
        <Input
          className={styles.input}
          disableUnderline
          fullWidth
          onChange={onChange('firstName')}
          placeholder="FIRST NAME"
          value={values.firstName}
        />
        <Input
          className={styles.input}
          disableUnderline
          fullWidth
          onChange={onChange('lastName')}
          placeholder="LAST NAME"
          value={values.lastName}
        />
      </div>
      <FormControl>
        <Select
          displayEmpty
          classes={{ root: styles.input }}
          onChange={onChange('ageGroup')}
          value={values.ageGroup || ''}
        >
          <MenuItem value="" disabled>AGE GROUP</MenuItem>
          <MenuItem value="25 years old or younger">25 years old or younger</MenuItem>
          <MenuItem value="26 to 35 years old">26 to 35 years old</MenuItem>
          <MenuItem value="36 to 45 years old">36 to 45 years old</MenuItem>
          <MenuItem value="46 to 55 years old">46 to 55 years old</MenuItem>
          <MenuItem value="56 years old or older">56 years old or older</MenuItem>
        </Select>
      </FormControl>
      <FormControl>
        <Select
          displayEmpty
          classes={{ root: styles.input }}
          onChange={onChange('gender')}
          value={values.gender || ''}
        >
          <MenuItem value="" disabled>GENDER</MenuItem>
          <MenuItem value="Male">Male</MenuItem>
          <MenuItem value="Female">Female</MenuItem>
          <MenuItem value="Other">Other</MenuItem>
        </Select>
      </FormControl>
    </div>
    <FormGroup row>
      <FormControlLabel
        control={(
          <Checkbox className={styles.checkbox} onChange={onChange('acceptedConditions')} checked={values.acceptedConditions} />
        )}
        label={(
          <Typography>
            I agree to the <Link to="/terms">Terms and Conditions</Link>
          </Typography>
        )}
      />
    </FormGroup>
  </form>
);

RegisterForm.propTypes = {
  onChange: func.isRequired,
  values: shape({
    acceptConditions: bool,
    ageGroup: string,
    firstName: string,
    gender: string,
    lastName: string,
  }),
};

RegisterForm.defaultProps = {
  values: {
    acceptConditions: false,
    ageGroup: '',
    firstName: '',
    gender: '',
    lastName: '',
  },
};

RegisterForm.isComplete = ({ firstName, lastName, ageGroup, gender, acceptedConditions }) => (
  !!firstName && !!lastName && !!ageGroup && !!gender && acceptedConditions
);

export default RegisterForm;
