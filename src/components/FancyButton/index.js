import React from 'react';
import Button from '@material-ui/core/Button';
import { bool, node } from 'prop-types';
import cs from 'classnames/bind';

import styles from './styles.module.css';
import rightArrow from './right-arrow.svg';

const cx = cs.bind(styles);

const FancyButton = ({ children, disabled, ...props }) => (
  <div className={cx('container', { disabled })}>
    {!disabled && <div className={styles.frame} />}
    <Button disabled={disabled} fullWidth className={styles.button} {...props}>
      {children}
      <img src={rightArrow} alt="arrow" width={32} style={{ marginLeft: '32px' }} />
    </Button>
  </div>
);

FancyButton.propTypes = {
  children: node,
  disabled: bool,
};

FancyButton.defaultProps = {
  children: null,
  disabled: false,
};

export default FancyButton;
