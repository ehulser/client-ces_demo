import { bool, number, string } from 'prop-types';
import cs from 'classnames/bind';
import moment from 'moment';
import React from 'react';
import Typography from '@material-ui/core/Typography';

import styles from './styles.module.css';

const cx = cs.bind(styles);

const Waypoint = ({
  color,
  duration,
  name,
  percent,
  rightAlign,
  showTime,
  time,
  width,
}) => (
  <div
    className={cx('waypoint', { rightAlign })}
    style={{
      [rightAlign ? 'right' : 'left']: `${percent}%`,
      width: width ? `${width}%` : null,
    }}
  >
    {showTime && (
      <React.Fragment>
        {duration
          ? (
            <div className={styles.row}>
              <Typography variant="overline" className={styles.small}>{moment(time).format('h:mm A')}</Typography>
              <Typography variant="overline" className={cx('small', 'pullRight')} style={{ color }}>{moment(time + duration).format('h:mm A')}</Typography>
            </div>
          )
          : <Typography variant="overline" className={styles.small}>{moment(time).format('h:mm A')}</Typography>
        }
      </React.Fragment>
    )}
    <div className={styles.circle} style={{ background: color }} />
    {!!duration
      && <div className={styles.overlay} style={{ background: color }} />
    }
    <Typography variant="overline" className={styles.small}>{name.toUpperCase()}</Typography>
  </div>
);

Waypoint.propTypes = {
  color: string,
  duration: number,
  name: string,
  percent: number,
  rightAlign: bool,
  showTime: bool,
  time: number,
  width: number,
};

Waypoint.defaultProps = {
  color: 'black',
  duration: 0,
  name: '',
  percent: 0,
  showTime: true,
  rightAlign: false,
  time: 0,
  width: null,
};

export default Waypoint;
