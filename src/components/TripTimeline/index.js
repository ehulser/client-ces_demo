import React from 'react';
import { arrayOf, string, number, shape, bool } from 'prop-types';

import Waypoint from './Waypoint';
import styles from './styles.module.css';

const TripTimeline = ({
  waypoints,
}) => {
  const lastIndex = waypoints.length - 1;
  const lastTime = waypoints[lastIndex].duration || waypoints[lastIndex].time;
  const firstTime = waypoints[0].time;
  const total = lastTime - firstTime;

  return (
    <div className={styles.container}>
      <div className={styles.bar} />
      {waypoints.map((waypoint, i) => {
        const width = waypoint.duration
          ? ((waypoint.duration - waypoint.time) / total) * 100
          : null;

        if (i === 0) {
          return <Waypoint key={waypoint.name} width={width} {...waypoint} />;
        }
        if (i === lastIndex) {
          return <Waypoint key={waypoint.name} width={width} rightAlign {...waypoint} />;
        }
        const percent = ((waypoint.time - firstTime) / total) * 100;
        return <Waypoint key={waypoint.name} width={width} percent={percent} {...waypoint} />;
      })}
    </div>
  );
};

export const waypointType = shape({
  color: string,
  duration: number,
  name: string.isRequired,
  percent: number,
  rightAlign: bool,
  time: number.isRequired,
  width: number,
});

TripTimeline.propTypes = {
  waypoints: arrayOf(waypointType),
};

TripTimeline.defaultProps = {
  waypoints: [],
};

export default TripTimeline;
