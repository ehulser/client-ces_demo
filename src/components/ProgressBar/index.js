import _range from 'lodash/range';
import { number } from 'prop-types';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import cs from 'classnames/bind';

import styles from './styles.module.css';

const cx = cs.bind(styles);

const ProgressBar = ({ maximum, value }) => (
  <div className={styles.container}>
    <div className={styles.inner}>
      <Typography className={styles.first}>
        {value.toString().padStart(2, '0')}
      </Typography>
      <div className={styles.progress}>
        {_range(maximum).map((step) => {
          const active = step < value;
          return <div key={step} className={cx('step', { active })} />;
        })}
      </div>
      <Typography className={styles.last}>
        {maximum.toString().padStart(2, '0')}
      </Typography>
    </div>
  </div>
);

ProgressBar.propTypes = {
  maximum: number,
  value: number,
};

ProgressBar.defaultProps = {
  maximum: 5,
  value: 1,
};

export default ProgressBar;
