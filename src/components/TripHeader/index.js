import React from 'react';
import { string } from 'prop-types';
import Typography from '@material-ui/core/Typography';

import styles from './styles.module.css';

const TripHeader = ({
  destination,
  firstName,
  profileImage,
}) => (
  <div className={styles.container}>
    {!!profileImage && <img className={styles.profileImage} src={profileImage} alt="" />}
    {!!(firstName && destination) && (
      <Typography>{firstName.toUpperCase()}&#39;S TRIP TO {destination.toUpperCase()}</Typography>
    )}
  </div>
);

TripHeader.propTypes = {
  destination: string,
  firstName: string,
  profileImage: string,
};

TripHeader.defaultProps = {
  destination: '',
  firstName: '',
  profileImage: '',
};

export default TripHeader;
