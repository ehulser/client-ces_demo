import _map from 'lodash/map';
import { arrayOf, func, shape, string, number } from 'prop-types';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import Icon from '@material-ui/core/Icon';
import React from 'react';
import Typography from '@material-ui/core/Typography';

import Rating from '../Rating';
import styles from './styles.module.css';

const RecommendationCard = ({
  onAdd,
  recommendation,
}) => (
  <Card className={styles.container}>
    <img src={recommendation.image_url} height={150} className={styles.image} alt="" />
    <Button className={styles.fab} variant="fab" color="primary" onClick={() => onAdd(recommendation.coordinates)}>
      <Icon>add</Icon>
    </Button>
    <div className={styles.copy}>
      <Typography variant="overline">
        {_map(recommendation.categories, ({ title }) => title).join(', ')}
      </Typography>
      <Typography variant="h5">{recommendation.name}</Typography>
      {recommendation.location.display_address.map(section => <Typography>{section}</Typography>)}
    </div>
    <div className={styles.stretch} />
    <div className={styles.feedback}>
      <div className={styles.row}>
        <Rating value={recommendation.rating} />
        <Typography>{recommendation.rating} ({recommendation.review_count})</Typography>
      </div>
      <div className={styles.stretch} />
      <div className={styles.row}>
        <Icon>flash_on</Icon>
        <Typography>
          {parseFloat(Math.round((recommendation.distance / 5280) * 100) / 100).toFixed(2)} mi away
        </Typography>
      </div>
    </div>
  </Card>
);

export const recommendationType = shape({
  categories: arrayOf(shape({
    title: string,
  })),
  coordinates: shape({
    latitude: number,
    longitude: number,
  }),
  distance: number,
  image_url: string,
  location: shape({
    display_address: arrayOf(string),
  }),
  name: string,
  rating: number,
  review_count: number,
});

RecommendationCard.propTypes = {
  onAdd: func,
  recommendation: recommendationType.isRequired,
};

RecommendationCard.defaultProps = {
  onAdd: null,
};

export default RecommendationCard;
