/* eslint-disable */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Star from '@material-ui/icons/Star';
import StarBorder from '@material-ui/icons/StarBorder';

const styles = {
  disabled: {
    pointerEvents: 'none'
  },
};

export default class Rating extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hoverValue: props.value,
    };
  }

  renderIcon(i) {
    const filled = i <= this.props.value;
    const hovered = i <= this.state.hoverValue;

    if ((hovered && !filled) || (!hovered && filled)) {
      return this.props.iconHoveredRenderer ? this.props.iconHoveredRenderer({
        ...this.props,
        index: i,
      }) : this.props.iconHovered
    } else if (filled) {
      return this.props.iconFilledRenderer ? this.props.iconFilledRenderer({
        ...this.props,
        index: i
      }) : this.props.iconFilled
    } else {
      return this.props.iconNormalRenderer ? this.props.iconNormalRenderer({
        ...this.props,
        index: i
      }) : this.props.iconNormal
    }
  }

  render () {
    const {
      disabled,
      iconFilled,
      iconHovered,
      iconNormal,
      tooltip,
      tooltipRenderer,
      tooltipPosition,
      tooltipStyles,
      iconFilledRenderer,
      iconHoveredRenderer,
      iconNormalRenderer,
      itemStyle,
      itemClassName,
      itemIconStyle,
      max,
      onChange,
      readOnly,
      style,
      value,
      ...other
    } = this.props

    const rating = []

    for (let i = 1; i <= max; i++) {
      rating.push(this.renderIcon(i))
    }

    return (
      <div
        style={this.props.disabled || this.props.readOnly ? {...styles.disabled, ...this.props.style} : this.props.style}
        {...other}
      >
        {rating}
      </div>
    )
  }
}

Rating.defaultProps = {
  disabled: false,
  iconFilled: <Star color="orange" />,
  iconHovered: <StarBorder color="orange" />,
  iconNormal: <StarBorder color="grey" />,
  tooltipPosition: 'bottom-center',
  max: 5,
  readOnly: false,
  value: 0
}

Rating.propTypes = {
  disabled: PropTypes.bool,
  iconFilled: PropTypes.node,
  iconHovered: PropTypes.node,
  iconNormal: PropTypes.node,
  tooltip: PropTypes.node,
  tooltipRenderer: PropTypes.func,
  tooltipPosition: PropTypes.string,
  tooltipStyles: PropTypes.object,
  iconFilledRenderer: PropTypes.func,
  iconHoveredRenderer: PropTypes.func,
  iconNormalRenderer: PropTypes.func,
  itemStyle: PropTypes.object,
  itemClassName: PropTypes.object,
  itemIconStyle: PropTypes.object,
  max: PropTypes.number,
  onChange: PropTypes.func,
  readOnly: PropTypes.bool,
  style: PropTypes.object,
  value: PropTypes.number
}
