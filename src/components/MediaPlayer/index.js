import { bool, number, string, shape } from 'prop-types';
import { PlayButton, Timer, Progress, VolumeControl } from 'react-soundplayer/components';
import { withSoundCloudAudio } from 'react-soundplayer/addons';
import cs from 'classnames/bind';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import React from 'react';
import Typography from '@material-ui/core/Typography';

import bg from './background.jpg';
import styles from './styles.module.css';

const cx = cs.bind(styles);

const MediaPlayer = ({ currentTime, track, playing, ...props }) => (
  <div className={styles.container} style={{ backgroundImage: `url(${bg})` }}>
    <div className={styles.content}>
      <div className={styles.track}>
        <Icon color="disabled" fontSize="large">music_note</Icon>
        <div className={styles.trackInfo}>
          <Typography variant="h5">{track ? track.title : 'Loading...'}</Typography>
          <Typography variant="h6">{!!track && track.user.username}</Typography>
        </div>
      </div>
      <div className={styles.controls}>
        <IconButton><Icon>shuffle</Icon></IconButton>
        <IconButton><Icon>skip_previous</Icon></IconButton>
        <IconButton classes={{ root: styles.controlIconButton }}>
          <div className={styles.controlButton}>
            <PlayButton
              className={cx('playButton', { alignPlay: !playing })}
              playing={playing}
              {...props}
            />
          </div>
        </IconButton>
        <IconButton><Icon>skip_next</Icon></IconButton>
        <IconButton><Icon>repeat</Icon></IconButton>
      </div>
      <div className={styles.volume}>
        <VolumeControl
          className={styles.volumeControl}
          buttonClassName={styles.volumeControlButton}
          rangeClassName={styles.volumeControlRange}
          {...props}
        />
      </div>
      <Typography
        className={styles.timer}
        style={{ left: `${track ? ((currentTime * 100000) / track.duration) : 0}%` }}
        variant="h5"
      >
        <Timer
          currentTime={currentTime}
          duration={track ? track.duration / 1000 : 0}
          {...props}
        />
      </Typography>
    </div>
    <Progress
      value={track ? ((currentTime * 100000) / track.duration) : 0}
      innerClassName={styles.progressBar}
      className={styles.progress}
      {...props}
    />
  </div>
);

MediaPlayer.propTypes = {
  currentTime: number,
  playing: bool,
  track: shape({
    duration: number,
    title: string,
  }),
};

MediaPlayer.defaultProps = {
  currentTime: 0,
  playing: false,
  track: null,
};

export default withSoundCloudAudio(MediaPlayer);
