import { func } from 'prop-types';
import Button from '@material-ui/core/Button';
import React from 'react';
import Typography from '@material-ui/core/Typography';

import leftArrow from './left-arrow.svg';

const BackButton = ({ onClick }) => (
  <Button onClick={onClick}>
    <img width={48} src={leftArrow} alt="back" style={{ marginRight: '32px' }} />
    <Typography variant="h4">Back</Typography>
  </Button>
);

BackButton.propTypes = {
  onClick: func,
};

BackButton.defaultProps = {
  onClick: null,
};

export default BackButton;
