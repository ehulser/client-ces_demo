import {
  CAPTURE_WEBCAM,
  RESET,
  SET_CURRENT_USER,
  SET_MEDIA,
  SET_FOOD,
  SET_DESTINATION,
} from '../actions';

const defaults = {
  acceptedConditions: false,
  ageGroup: '',
  destination: '',
  firstName: '',
  food: {},
  gender: '',
  lastName: '',
  media: {},
  profileImage: '',
  snapshots: [],
};

const reducer = (state = defaults, action) => {
  switch (action.type) {
    case CAPTURE_WEBCAM: {
      const { image } = action;
      const { snapshots } = state;
      const newSnapshots = [...snapshots, image];
      if (newSnapshots.length === 5) {
        console.log(JSON.stringify(newSnapshots));
      }
      return { ...state, profileImage: image, snapshots: newSnapshots };
    }
    case RESET: {
      return { ...defaults };
    }
    case SET_CURRENT_USER: {
      const { user } = action;
      return { ...state, ...user };
    }
    case SET_DESTINATION: {
      const { destination } = action;
      return { ...state, destination };
    }
    case SET_FOOD: {
      const { food } = action;
      return { ...state, food };
    }
    case SET_MEDIA: {
      const { media } = action;
      return { ...state, media };
    }
    default: return state;
  }
};

export default reducer;
