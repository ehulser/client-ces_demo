import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import map from './map';
import soundcloud from './soundcloud';
import user from './user';

const reducerFactory = history => combineReducers({
  map,
  router: connectRouter(history),
  soundcloud,
  user,
});

export default reducerFactory;
