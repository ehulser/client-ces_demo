export const trackUrls = {
  JAZZ: [
    'https://soundcloud.com/npsjazz/moose-and-wolves-the-nps-centennial-band-feat-jeff-woolin',
  ],
};

const defaults = {
  clientId: 'pQsBqVzC5XL0PCapo5rapEFqL7ogk13U',
  currentTrackUrl: trackUrls.JAZZ[0],
};

const reducer = (state = defaults, action) => {
  switch (action.type) {
    default: return state;
  }
};

export default reducer;
