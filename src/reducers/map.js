import {
  ADD_WAYPOINT,
  RESET,
  SET_DURATIONS,
  SET_MAP_INFO,
  SET_FOOD_RECOMMENDATIONS,
  SET_WAYPOINT_LABELS,
  SET_WAYPOINTS,
  SET_TRIP_PERCENT_COMPLETE,
  SET_TRIP_POSITION,
  SET_SHOW_RECOMMENDATIONS,
} from '../actions';

const defaults = {
  durations: [],
  labels: [],
  percentComplete: 0,
  position: null,
  recommendations: null,
  showRecommendations: false,
  waypoints: [],
};

const reducer = (state = defaults, action) => {
  switch (action.type) {
    case ADD_WAYPOINT: {
      const { longitude, latitude } = action;
      const { waypoints } = state;
      return {
        ...state,
        showRecommendations: false,
        waypoints: [waypoints[0], [latitude, longitude], ...waypoints.slice(1)],
      };
    }
    case RESET: {
      return { ...defaults };
    }
    case SET_DURATIONS: {
      const { durations } = action;
      return { ...state, durations };
    }
    case SET_MAP_INFO: {
      const { info } = action;
      return { ...state, ...info };
    }
    case SET_WAYPOINT_LABELS: {
      const { labels } = action;
      return { ...state, labels };
    }
    case SET_WAYPOINTS: {
      const { waypoints } = action;
      return { ...state, waypoints };
    }
    case SET_TRIP_POSITION: {
      const { position } = action;
      return { ...state, position };
    }
    case SET_TRIP_PERCENT_COMPLETE: {
      const { percent } = action;
      return { ...state, percentComplete: percent };
    }
    case SET_FOOD_RECOMMENDATIONS: {
      const { recommendations } = action;
      return { ...state, recommendations, showRecommendations: true };
    }
    case SET_SHOW_RECOMMENDATIONS: {
      const { showRecommendations } = action;
      return { ...state, showRecommendations };
    }
    default: return state;
  }
};

export default reducer;
