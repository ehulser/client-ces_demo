import _reduce from 'lodash/reduce';
import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import {
  BUILD_ROUTE,
  CAPTURE_WEBCAM,
  REGISTER_USER,
  RESET,
  REQUEST_FOOD_RECOMMENDATIONS,
  setCurrentUser,
  setMapInfo,
  setFoodRecommendations,
} from './actions';
import { getFoodRecommendations, getWaypoints } from './api';
import { normalizeRouteApiResponse } from './normalizers';
import {
  DEST_VEGAS,
  DEST_ANTELOPE_CANYON,
  DEST_GRAND_CANYON,
  DEST_HOOVER_DAM,
} from './constants';

const COORDINATES = {
  [DEST_ANTELOPE_CANYON]: { latitude: 36.8619, longitude: -111.3743 },
  [DEST_GRAND_CANYON]: { latitude: 36.1070, longitude: -112.1130 },
  [DEST_HOOVER_DAM]: { latitude: 36.0161, longitude: -114.7377 },
  [DEST_VEGAS]: { latitude: 36.1699, longitude: -115.1398 },
};

const getFood = ({ user }) => _reduce(user.food, (acc, favorite, type) => {
  if (favorite) return [...acc, type];
  return acc;
}, []);

function* captureWebcamSaga(action) {
  const { image } = action;
}

function* buildRouteSaga(action) {
  const { startName, destinationName } = action;
  const start = COORDINATES[startName];
  const destination = COORDINATES[destinationName];
  const response = yield call(getWaypoints, start, destination);
  const { durations, waypoints } = yield call(normalizeRouteApiResponse, response);
  const labels = waypoints.map((_, i) => {
    if (i === 0) {
      return startName;
    }
    if (i === waypoints.length - 1) {
      return destinationName;
    }
    return 'BYTON Fast Charge';
  });
  yield put(setMapInfo({ durations, labels, waypoints }));
}

function* getRecommendationsSaga(action) {
  const { position } = action;
  const food = yield select(getFood);
  const { data } = yield call(getFoodRecommendations, position[0], position[1], food);
  yield put(setFoodRecommendations([data, data, data, data]));
}

function* resetSaga() {
  yield put(push('/'));
}

function* registerUserSaga(action) {
  const { user } = action;
  console.log('TODO: register user to API:', user);
  yield put(setCurrentUser(user));
  yield put(push('/destination'));
}

export default function* rootSaga() {
  return yield all([
    takeLatest(BUILD_ROUTE, buildRouteSaga),
    takeLatest(CAPTURE_WEBCAM, captureWebcamSaga),
    takeLatest(REGISTER_USER, registerUserSaga),
    takeLatest(RESET, resetSaga),
    takeLatest(REQUEST_FOOD_RECOMMENDATIONS, getRecommendationsSaga),
  ]);
}
